# Paso de atributos con INPUT y OUTPUT

Aquí se muestra como pasar parametros usando los decoradores INPUT y OUTPUT

### **INPUT()** 

Input es un decorador que tiene como propiedad la entrada de datos. Se puede proporcional un nombre que acompañe al decorador sin embargo no es necesario. 


* Decorador con nombre

```Typescript
  @Input() nombre: string;
```

* Decorador sin nombre 

```Typescript
  @Input('cuenta-id') id: string;
```

### **OUTPUT()**

El decorador **output** utiliza un eventos de componenete sin estado usando **EventEmitter** API, estos juntos permiten emitir cambios o nombres de eventos personalizados desde un componente en Angular. 

### **EventEmitter**

Se debe importar y vincular una nueva instancia de EventEmitter en uel componente a utilizar. 

```TypeScript 

@Component({
    selector: 'formulario-login',
    templateUrl: './formulario-login.component.html',
    styleUrls: ['./formulario-login.component.css']
})

export class FormularioLoginComponent implements OnInit{

    @Output() enviarCredenciales: EventEmitter< credencialesLogin | boolean >= new EventEmitter ()

```

Finalmente el componente.html en el cual se va a usar el output debera escuchar el evento 

```HTML 
  <formulario-login (enviarCredenciales)="escucharEventoFormulario($event)"> 
```


<a href="https://twitter.com/alimonse29" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @alimonse29 </a><br>
